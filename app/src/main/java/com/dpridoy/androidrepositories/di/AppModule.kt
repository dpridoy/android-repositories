package com.dpridoy.androidrepositories.di

import com.dpridoy.androidrepositories.BuildConfig
import com.dpridoy.androidrepositories.data_source.ApiDataSource
import com.dpridoy.androidrepositories.data_source.ApiService
import com.dpridoy.androidrepositories.utils.AppConstants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Singleton
    @Provides
    fun provideDataSource(apiService: ApiService) = ApiDataSource(apiService)

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, httpClient: OkHttpClient) : Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(httpClient)
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun getHttpClient(headerInterceptor: Interceptor): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY // Covers both Header & Body
        } else {
            HttpLoggingInterceptor.Level.BASIC
        }
        return OkHttpClient.Builder()
            .addInterceptor(headerInterceptor)
            .addInterceptor(interceptor)
            .build()
    }

    /*Adding Package As header*/
    @Provides
    fun provideHeaderInterceptor(): Interceptor = Interceptor { chain ->
        val request = chain.request()
        val builder = request.newBuilder()
        builder.addHeader(AppConstants.KEY_HEADER_PACKAGE, AppConstants.KEY_HEADER_PACKAGE_VALUE)
        chain.proceed(builder.build())
    }
}