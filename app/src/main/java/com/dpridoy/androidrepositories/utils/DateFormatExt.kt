package com.dpridoy.androidrepositories.utils

import java.text.SimpleDateFormat
import java.util.Locale

const val FORMAT_yyyy_MM_dd_T_HH_mm_ss_Z = "yyyy-MM-dd'T'HH:mm:ss'Z'"
const val FORMAT_dd_MM_yy_HH_mm = "dd-MM-yy HH:mm"

fun String.format(
    inFormat: String,
    outFormat: String,
    destLocale: Locale? = Locale.getDefault()
): String {
    return try {
        val inFormatter = SimpleDateFormat(inFormat, Locale.ENGLISH)
        val outFormatter = SimpleDateFormat(outFormat, destLocale)
        outFormatter.format(inFormatter.parse(this)!!)
    } catch (e: Exception) {
        e.printStackTrace()
        ""
    }
}

fun String.formatDate(
    destLocale: Locale? = Locale.getDefault()
): String? {
    if (this.isEmpty())
        return null
    return format(
        inFormat = FORMAT_yyyy_MM_dd_T_HH_mm_ss_Z,  // Available in API 24+
        outFormat = FORMAT_dd_MM_yy_HH_mm,
        destLocale = destLocale
    )
}