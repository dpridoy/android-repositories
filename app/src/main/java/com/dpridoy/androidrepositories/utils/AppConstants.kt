package com.dpridoy.androidrepositories.utils

import com.dpridoy.androidrepositories.BuildConfig

object AppConstants {

    const val KEY_HEADER_PACKAGE = "package"
    const val KEY_HEADER_PACKAGE_VALUE = BuildConfig.APPLICATION_ID
    const val API_QUERY_PARAM_VALUE_1 = "android"
    const val API_QUERY_PARAM_VALUE_2 = "stars"

    const val INTENT_KEY_ITEM = "ITEM"
}