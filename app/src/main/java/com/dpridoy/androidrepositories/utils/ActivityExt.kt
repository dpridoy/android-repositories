package com.dpridoy.androidrepositories.utils

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable

fun <T : Activity> Activity.navigateTo(
    targetActivityClass: Class<T>,
    withIntentData: Bundle? = null,
    clearTask: Boolean = false
) {
    try {
        // creating intent
        val intent = Intent(this, targetActivityClass)

        // attaching intent data
        withIntentData?.let { bundle ->
            for (key in bundle.keySet()) {
                when (val value = bundle.get(key)) {
                    is String -> intent.putExtra(key, value)
                    is Boolean -> intent.putExtra(key, value)
                    is Int -> intent.putExtra(key, value)
                    is Double -> intent.putExtra(key, value)
                    is Float -> intent.putExtra(key, value)
                    is Parcelable -> intent.putExtra(key, value)
                    is ArrayList<*> -> intent.putExtra(key, value)
                }
            }
        }

        // adding flags
        intent.flags = if (clearTask) {
            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        } else Intent.FLAG_ACTIVITY_CLEAR_TOP

        // start activity
        startActivity(intent)
    } catch (e: Exception) {
        // getString(R.string.something_went_wrong).showToast()
    }
}