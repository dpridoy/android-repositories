package com.dpridoy.androidrepositories.dto

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Items(
    @SerializedName("name")
    val name: String ?= null,
    @SerializedName("full_name")
    val fullName: String ?= null,
    @SerializedName("updated_at")
    val updatedAt: String ?= null,
    @SerializedName("owner")
    val owner: Owner,
    @SerializedName("homepage")
    val homepage: String ?= null,
    @SerializedName("description")
    val description: String ?= null
): Parcelable
