package com.dpridoy.androidrepositories.dto

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Owner(
    @SerializedName("avatar_url")
    val avatarUrl: String ?= null,
    @SerializedName("organizations_url")
    val organizationsUrl: String ?= null
): Parcelable
