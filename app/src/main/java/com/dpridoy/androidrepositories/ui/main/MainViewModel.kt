package com.dpridoy.androidrepositories.ui.main

import android.app.Application
import androidx.lifecycle.ViewModel
import com.dpridoy.androidrepositories.data_source.ApiDataSource
import com.dpridoy.androidrepositories.data_source.performApiCall
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val apiDataSource: ApiDataSource,
    private val context: Application
) : ViewModel(){

    fun getRepositories() = performApiCall(context){apiDataSource.getRepositories()}
}