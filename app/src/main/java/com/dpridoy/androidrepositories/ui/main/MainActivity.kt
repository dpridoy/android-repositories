package com.dpridoy.androidrepositories.ui.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.dpridoy.androidrepositories.base.BaseActivity
import com.dpridoy.androidrepositories.data_source.Resource
import com.dpridoy.androidrepositories.databinding.ActivityMainBinding
import com.dpridoy.androidrepositories.dto.Items
import com.dpridoy.androidrepositories.dto.RepositoryResponse
import com.dpridoy.androidrepositories.ui.details.DetailsActivity
import com.dpridoy.androidrepositories.utils.AppConstants
import com.dpridoy.androidrepositories.utils.navigateTo
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val items: ArrayList<Items> = arrayListOf()

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getData()
    }

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    private fun getData() {
        viewModel.getRepositories().observe(this) {
            when (it.status) {
                Resource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    val error = it.message
                    showToast(this, error!!)
                }

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }

                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    val response = it.data as RepositoryResponse
                    response.items?.let { it1 -> items.addAll(it1) }
                    setItems(items)
                }

                Resource.Status.SERVER_ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    showToast(this, "Server Error!!")
                }
            }
        }
    }

    private fun setItems(items: java.util.ArrayList<Items>) {
        val rvLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        binding.rvRepo.apply {
            layoutManager = rvLayoutManager
            isNestedScrollingEnabled = false
            adapter = RepositoryAdapter(
                items,
                ::onItemClick
            )
            isFocusable = false
            isFocusableInTouchMode = false
        }
    }

    private fun onItemClick(pos: Int) {
        items[pos].let {
            val bundle = Bundle()
            bundle.putParcelable(AppConstants.INTENT_KEY_ITEM, it)
            navigateTo(DetailsActivity::class.java, bundle)
        }
    }
}
