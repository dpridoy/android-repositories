package com.dpridoy.androidrepositories.ui.details

import android.os.Bundle
import com.dpridoy.androidrepositories.base.BaseActivity
import com.dpridoy.androidrepositories.databinding.ActivityDetailsBinding
import com.dpridoy.androidrepositories.dto.Items
import com.dpridoy.androidrepositories.utils.AppConstants
import com.dpridoy.androidrepositories.utils.formatDate
import com.dpridoy.androidrepositories.utils.loadImageWithGlide

class DetailsActivity : BaseActivity<ActivityDetailsBinding>() {

    private var item: Items? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setData()
    }

    override fun getViewBinding(): ActivityDetailsBinding {
        return ActivityDetailsBinding.inflate(layoutInflater)
    }

    private fun setData() {
        item = intent?.getParcelableExtra(AppConstants.INTENT_KEY_ITEM)
        binding.name.text = item?.name
        binding.desc.text = item?.description
        item?.updatedAt?.formatDate()?.let { binding.updated.text = it }
        binding.image.loadImageWithGlide(item?.owner?.avatarUrl)
    }
}