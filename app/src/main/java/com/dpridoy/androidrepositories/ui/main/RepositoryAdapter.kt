package com.dpridoy.androidrepositories.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dpridoy.androidrepositories.databinding.ItemRepoBinding
import com.dpridoy.androidrepositories.dto.Items

class RepositoryAdapter (
    private var items: List<Items>,
    private var onItemClick: ((Int) -> Unit)? = null
) : RecyclerView.Adapter<RepositoryAdapter.RepoViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view = ItemRepoBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return RepoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        holder.bind(items[position],position)
    }

    inner class RepoViewHolder(
        private val binding: ItemRepoBinding
    ): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Items, pos: Int){
            binding.title.text = item.name
            binding.desc.text = item.description
            binding.root.setOnClickListener { onItemClick?.invoke(pos) }
        }
    }
}
