package com.dpridoy.androidrepositories.data_source

import com.dpridoy.androidrepositories.dto.RepositoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("search/repositories")
    suspend fun getRepositories(
        @Query("q") q: String,
        @Query("sort") sort: String
    ) : Response<RepositoryResponse>
}