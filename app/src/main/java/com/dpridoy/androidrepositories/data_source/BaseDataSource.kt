package com.dpridoy.androidrepositories.data_source

import org.json.JSONObject
import retrofit2.Response

abstract class BaseDataSource {
    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<*> {

        try {

            val response = call()
            when(response.code()){

                Resource.Status.SUCCESS.value -> {
                    val body = response.body()
                    if (body != null) return Resource.success(body)
                }
                // Error can be handled with proper error model according to backend
                Resource.Status.ERROR.value -> {
                    val errorBody = JSONObject(response.errorBody()!!.charStream().readText())
                    val res = Resource.error(errorBody.getString("message"), null)
                    return res
                }
                Resource.Status.SERVER_ERROR.value -> {
                    val errorBody = JSONObject(response.errorBody()!!.charStream().readText())
                    return Resource.serverError(errorBody.getString("message"), null)
                }
            }

        } catch (e: Exception) {
            return Resource.noInternet("Exception ",e)
        }

        return Resource.noInternet("Exception ","No Internet")
    }

    private fun <T> error(): Resource<T> {
        return Resource.noInternet("")
    }
}