package com.dpridoy.androidrepositories.data_source

import com.dpridoy.androidrepositories.utils.AppConstants
import javax.inject.Inject

class ApiDataSource @Inject constructor(private val apiService: ApiService) : BaseDataSource() {

    suspend fun getRepositories() = getResult(call = {
        apiService.getRepositories(
            AppConstants.API_QUERY_PARAM_VALUE_1,
            AppConstants.API_QUERY_PARAM_VALUE_2
        )
    })
}