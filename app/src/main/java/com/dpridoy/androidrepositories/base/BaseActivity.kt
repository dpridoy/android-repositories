package com.dpridoy.androidrepositories.base

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewbinding.ViewBinding
import com.dpridoy.androidrepositories.R
import com.dpridoy.androidrepositories.data_source.ApiDataSource
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

abstract class BaseActivity<T : ViewBinding> : AppCompatActivity() {

    @Inject
    lateinit var apiDataSource: ApiDataSource

    protected lateinit var binding: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewBinding()
        setContentView(binding.root)
    }

    abstract fun getViewBinding(): T


    fun showToast(context: Context, message: String){
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show()
    }

    fun showErrorSnackBar(view: View, message: String, context: Context) {
        val sb: Snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
        val sbView = sb.view
        sbView.setBackgroundColor(
            ContextCompat.getColor(context,
                R.color.white))
        sb.show()
    }

    fun hideKeyboard(){
        this.currentFocus?.let { view ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }



    fun restartActivity() {
        val intent = Intent(this, javaClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        restartActivity()
    }

}